package com.cop.abhinav_kushagra.complaint_system;

import org.json.JSONArray;

/**
 * Created by Abhinav-kumar-Shaw on 3/29/2016.
 */
public class ResponseJsonArrayClass {
    JSONArray response_class;
    int error_code;

    public ResponseJsonArrayClass(){
        this.error_code=0;
        this.response_class=new JSONArray();
    }
    public ResponseJsonArrayClass(JSONArray object,int error_code){
        this.response_class=object;
        this.error_code=error_code;
    };

    public void setJsonResponse(JSONArray object){
        this.response_class=object;
    }
    public void setError_code(int error_code){
        this.error_code=error_code;
    }
    public JSONArray getJsonResponse(){
        return this.response_class;
    }

    public int getError_code(){
        return error_code;
    }
}
