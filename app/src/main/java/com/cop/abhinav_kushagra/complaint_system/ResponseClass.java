package com.cop.abhinav_kushagra.complaint_system;

import org.json.JSONObject;

public class ResponseClass {
    JSONObject response_class;
    int error_code;

    public ResponseClass(){
        this.error_code=0;
        this.response_class=new JSONObject();
    }
    public ResponseClass(JSONObject object,int error_code){
        this.response_class=object;
        this.error_code=error_code;
    };

    public void setJsonResponse(JSONObject object){
        this.response_class=object;
    }
    public void setError_code(int error_code){
        this.error_code=error_code;
    }
    public JSONObject getJsonResponse(){
        return this.response_class;
    }

    public int getError_code(){
        return error_code;
    }
}
