package com.cop.abhinav_kushagra.complaint_system;


import java.io.Serializable;
import java.util.Date;

public class Complaints implements Serializable {
    int cid ;
    int uid ;
    int type ;
    int subtype ;
    String title ;
    String description ;
    Boolean anonymous ;
    int visiblity_level ;
    Boolean urgent;
    String submit_date;
    String resolved_date;
    int no_of_upvotes;
    int status;
    String offcial_reply;

//        cid Complaint ID
//        uid User issuing the Complaint
//        type Institute or Hostel or Indivisual Level Complaint
//        subtype e.g In hostel level whether it is mess complaint etc.
//        title Heading for the Compalint
//        description Complete description of the complaint
//        anonymous Boolean for remaining anonymous to the complaint reciever
//        visiblity level Visiblity level of the complaints ( Visible to all people, Visible
//        only to faculties)
//        urgent True if the complaint is urgent
//        date time submit Date and time of submitting the complaint
//        date time resolve Date and time when the complaint is closed
//        no of upvotes Number of upvotes for the publicly visible complaints
//        status status of the complaint (solved or not)
//        official reply The offcial reply from the authority


    public Complaints(Boolean anonymous, int cid, String description, int no_of_upvotes, String offcial_reply, String resolved_date, int status, String submit_date, int subtype, String title, int type, int uid, Boolean urgent, int visiblity_level) {
        this.anonymous = anonymous;
        this.cid = cid;
        this.description = description;
        this.no_of_upvotes = no_of_upvotes;
        this.offcial_reply = offcial_reply;
        this.resolved_date = resolved_date;
        this.status = status;
        this.submit_date = submit_date;
        this.subtype = subtype;
        this.title = title;
        this.type = type;
        this.uid = uid;
        this.urgent = urgent;
        this.visiblity_level = visiblity_level;
    }
}
