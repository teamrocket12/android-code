package com.cop.abhinav_kushagra.complaint_system;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;

public class CreateComplaintActivity extends AppCompatActivity {

    Complaints complaints;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_complaint);
    }

    public void onCheckboxClicked(View view) {
        // Is the view now checked?
        boolean checked = ((CheckBox) view).isChecked();

        // Check which checkbox was clicked
        switch(view.getId()) {
            case R.id.checkbox_public:
                if (checked)

                // Put some meat on the sandwich
                else
                // Remove the meat
                break;
            case R.id.checkbox_anonymous:
                if (checked)
                // Cheese me
                else
                // I'm lactose intolerant
                break;
        }
    }
}
