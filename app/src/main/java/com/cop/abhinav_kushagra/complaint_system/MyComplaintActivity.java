package com.cop.abhinav_kushagra.complaint_system;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MyComplaintActivity extends AppCompatActivity {

    static class newsViewHolder {
        TextView title;
        TextView description;
        TextView createdate;
        TextView status;
        TextView officialreply;
        int pos; //to store the position of the item within the list
    }

    NetworkFunctions net=new NetworkFunctions();

    TextView navigation_name,navigation_entryno,navigation_email;
    ListView newsList,drawerList;
    LinearLayout linearUser;

    String[] drawerItemstudent={"Home","Pending Complaints","Notification","Logout"};
    String[] drawerItemProf={"Home","Notification","Logout"};
    ArrayList<Complaints> newsListFetched =new ArrayList<Complaints>();
    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_complaint);
        setTitle("My Complaints");
        Intent intent=getIntent();
        user=(User) intent.getSerializableExtra("UserData");

        navigation_name=(TextView) findViewById(R.id.nav_username);
        navigation_entryno=(TextView) findViewById(R.id.nav_entryno);
        navigation_email=(TextView) findViewById(R.id.nav_email);
        newsList = (ListView) findViewById(R.id.courseList);
        drawerList = (ListView) findViewById(R.id.drawerList);
        linearUser = (LinearLayout) findViewById(R.id.linearUser);

        navigation_name.setText(user.firstName + " " + user.lastName);
        navigation_entryno.setText(user.entryNumber);
        navigation_email.setText(user.email);
        //courseListFetched.add(new Courses("", "Fetching"));

        linearUser.setOnClickListener(new LinearLayout.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MyComplaintActivity.this, UserActivity.class);
                intent.putExtra("userData", user);
                startActivity(intent);

            }
        });

        if (user.type==0){
            drawerList.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,drawerItemstudent));
            drawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent;
                    switch (position) {
                        case 0:
                            intent = new Intent(MyComplaintActivity.this, ComplaintFeed.class);
                            intent.putExtra("UserData",user);
                            startActivity(intent);
                            break;
                        case 1:
                            intent = new Intent(MyComplaintActivity.this, NotificationActivity.class);
                            startActivity(intent);
                            break;
                        case 2:
                            intent = new Intent(MyComplaintActivity.this, LoginActivity.class);
                            logout();
                            // check again
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            finish();
                            break;
                    }

                }
            });
        }else{
            drawerList.setAdapter(new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,drawerItemProf));
            drawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent;
                    switch (position) {
                        case 0:
                            intent = new Intent(MyComplaintActivity.this, ComplaintFeed.class);
                            intent.putExtra("UserData",user);
                            startActivity(intent);
                            break;
                        case 1:
                            intent = new Intent(MyComplaintActivity.this, PendingComplaintActivity.class);
                            intent.putExtra("UserData",user);
                            startActivity(intent);
                            break;
                        case 2:
                            intent = new Intent(MyComplaintActivity.this, NotificationActivity.class);
                            startActivity(intent);
                            break;
                        case 3:
                            intent = new Intent(MyComplaintActivity.this, LoginActivity.class);
                            logout();
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                            finish();
                            break;
                    }

                }
            });
        }

        newsList.setAdapter(new BaseAdapter() {

            LayoutInflater inflater = LayoutInflater.from(MyComplaintActivity.this);

            @Override
            public int getCount() {
                return newsListFetched.size();
            }

            @Override
            public Object getItem(int position) {
                return newsListFetched.get(position);
            }

            @Override
            public long getItemId(int position) {
                return position;
            }

            @Override
            public void notifyDataSetChanged() {
                super.notifyDataSetChanged();
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                newsViewHolder viewHolder;
                if (convertView == null) {
                    convertView = inflater.inflate(R.layout.layout_newsfeed, parent, false);
                    viewHolder = new newsViewHolder();
                    viewHolder.title = (TextView) convertView.findViewById(R.id.complaintitle);
                    viewHolder.description = (TextView) convertView.findViewById(R.id.description);
                    viewHolder.createdate = (TextView) convertView.findViewById(R.id.createddate);
                    viewHolder.status = (TextView) convertView.findViewById(R.id.status);
                    viewHolder.officialreply = (TextView) convertView.findViewById(R.id.officialreply);
                    convertView.setTag(viewHolder);
                } else {
                    viewHolder = (newsViewHolder) convertView.getTag();
                }
                //String currentListData = getItem(position);
                viewHolder.title.setText(newsListFetched.get(position).title);
                viewHolder.description.setText(newsListFetched.get(position).description);
                viewHolder.createdate.setText(newsListFetched.get(position).submit_date.toString());
                switch (newsListFetched.get(position).status){
                    case 1:{
                        viewHolder.status.setText("Unresolved");
                        break;
                    }
                    case 2:{
                        viewHolder.status.setText("Resolved from authority");
                        break;
                    }
                    case 3:{
                        viewHolder.status.setText("Resolved");
                        break;
                    }
                    default:

                }                viewHolder.officialreply.setText(newsListFetched.get(position).offcial_reply);


                viewHolder.pos = position;
                return convertView;
            }
        });

        newsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent = new Intent(MyComplaintActivity.this, ComplaintActivity.class);
                intent.putExtra("complaintid", newsListFetched.get(position).cid);
                intent.putExtra("Userdata",user);
                startActivity(intent);

            }
        });

        populateComplaintList();

    }

    // function to get all courses
    public void populateComplaintList(){

        net.getmycomplaints(
                this, new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        newsListFetched = new ArrayList<Complaints>();
                        Log.d("ComplaintList_response", response.toString());
                        try {
                            int i = 0;
                            while (i < response.length()) {
                                JSONObject jsObj = response.getJSONObject(i);
                                newsListFetched.add(new Complaints(jsObj.getBoolean("anonymous"),
                                        jsObj.getInt("complaints_id"),
                                        jsObj.getString("description"),
                                        jsObj.getInt("no_of_upvotes"),
                                        jsObj.getString("official_reply"),
                                        jsObj.getString("date_time_resolve"),
                                        jsObj.getInt("status"),
                                        jsObj.getString("date_time_submit"),
                                        jsObj.getInt("c_subtype"),
                                        jsObj.getString("title"),
                                        jsObj.getInt("c_type"),
                                        jsObj.getInt("user"),
                                        jsObj.getBoolean("urgent"),
                                        jsObj.getInt("visiblity_level")
                                ));
                                i++;
                            }
                            ((BaseAdapter) newsList.getAdapter()).notifyDataSetChanged();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(MyComplaintActivity.this, "Network Error!!", Toast.LENGTH_LONG).show();
                        Log.d("Login", "error");
                    }
                },user.username);
    }

    public void logout(){

        SaveSharedPreference.clear(MyComplaintActivity.this);
//        net.logoutresponse(
//                this, new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//
//                        Log.d("Logout_response", response.toString());
//
//                    }
//                }, new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        Toast.makeText(CourseListActivity.this, "Network Error", Toast.LENGTH_LONG).show();
//                        Log.d("Login", "error");
//                    }
//                });
    }
}
