package com.cop.abhinav_kushagra.complaint_system;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class SplashScreen extends AppCompatActivity {
    NetworkFunctions net=new NetworkFunctions();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                final String storedUsername, storedPassword;

                if (SaveSharedPreference.getUserName(SplashScreen.this).length() == 0 || SaveSharedPreference.getPassword(SplashScreen.this).length() == 0) {
                    Intent intent = new Intent(SplashScreen.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                }
                // executed when user have pressed keep me log in
                else {
                    String temp = SaveSharedPreference.getPrefUnPass(SplashScreen.this);
                    storedUsername = SaveSharedPreference.getUserName(SplashScreen.this);
                    storedPassword = SaveSharedPreference.getPassword(SplashScreen.this);
                    Log.d("Splash", storedUsername + storedPassword);
//                    net.loginresponse(temp.split(",")[0], temp.split(",")[1], SplashScreen.this, new Response.Listener<JSONObject>() {
//                        @Override
//                        public void onResponse(JSONObject response) {
//                            Intent intent = new Intent(SplashScreen.this, CourseListActivity.class);
//                            Log.d("Login", response.toString());
//                            String lastname = "", firstname = "", entrynumber = "", id = "", email = "", uusername;
//                            try {
//                                lastname = response.getJSONObject("user").getString("last_name");
//                                firstname = response.getJSONObject("user").getString("first_name");
//                                entrynumber = response.getJSONObject("user").getString("entry_no");
//                                email = response.getJSONObject("user").getString("email");
//                                id = response.getJSONObject("user").getString("id");
//                                uusername = response.getJSONObject("user").getString("username");
//                                intent.putExtra("UserData", new User(firstname, lastname, id, entrynumber, email, uusername, response.getJSONObject("user").getInt("type_")));
////                                SaveSharedPreference.setUserName(SplashScreen.this, storedUsername);
////                                SaveSharedPreference.setPassword(SplashScreen.this, storedPassword);
//                                startActivity(intent);
//                                finish();
//                            } catch (JSONException e) {
//                                intent = new Intent(SplashScreen.this, LoginActivity.class);
//                                startActivity(intent);
//                                finish();
//                                Toast.makeText(SplashScreen.this, "Invalid Username or Password", Toast.LENGTH_LONG).show();
//                                SaveSharedPreference.clear(SplashScreen.this);
//                                e.printStackTrace();
//                            }
//
//                        }
//                    }, new Response.ErrorListener() {
//                        @Override
//                        public void onErrorResponse(VolleyError error) {
//                            Log.d("Splash", "error");
//                            Intent intent = new Intent(SplashScreen.this, LoginActivity.class);
//                            startActivity(intent);
//                            finish();
//                            Toast.makeText(SplashScreen.this, "Network Error!!", Toast.LENGTH_LONG).show();
//                        }
//                    });

                }
            }
        }, 1000);
    }
    }

