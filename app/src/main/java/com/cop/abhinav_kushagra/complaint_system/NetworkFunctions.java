package com.cop.abhinav_kushagra.complaint_system;
import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import  org.json.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class NetworkFunctions {
    private String IP_Addr;
    private int port;
    private boolean result;
    private JSONObject jsonObject;
    //private static RequestQueue queue;

    public NetworkFunctions(){
        IP_Addr="192.168.173.1";
        port=8000;
        result=false;
        jsonObject=new JSONObject();
        //queue=Volley.newRequestQueue(c);
    }

    public void setObject(JSONObject o){
        this.jsonObject=o;
    }

    public ResponseClass login(String username, String password, Context c){
        // Instantiate the RequestQueue.
        //RequestQueue queue=Volley.newRequestQueue(c);
        final ResponseClass resp=new ResponseClass();
        String url ="http://"+this.IP_Addr+":"+this.port+"/default/login.json?userid="+username+"&password="+password;
        User user;
        // resp.setResponse_class();
        Log.d("responseURL", url);
        this.result=false;
        final String[] message = new String[1];
        // This code sends volley request and listens to response

        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                        String responseSuccess;

                        String responseMessage = "";
                        try {
                            responseSuccess = response.getString("success");
                            message[0] =responseSuccess;
                            Log.d("response", (response).toString());

                            if (responseSuccess.equals("true")) {
                                resp.setJsonResponse(response);
                                //populateCourseList();
                                Log.d("result", "success");
                            } else {
                                resp.setError_code(1);
                                Log.d("result", "failure");
                            }
                            Log.d("response", response.toString());

                        } catch (Throwable t) {

                        }
                        //behave according to the recieved response code
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("responseError", "C" + error.toString());
                        resp.setError_code(2);
                    }
                });



        ComplaintSystem.getInstance().addToRequestQueue(jsObjRequest);
        Log.d("responseLogin","Yolo"+ this.result+resp.getJsonResponse().toString());

        return resp;
    }


    public void loginresponse(String username, String password, Context c,Response.Listener succesListener,Response.ErrorListener errorListener){
        // Instantiate the RequestQueue.
        //RequestQueue queue=Volley.newRequestQueue(c);

        String url ="http://"+this.IP_Addr+":"+this.port+"/default/login.json?userid="+username+"&password="+password;

        // resp.setResponse_class();
        Log.d("responseURL",url);
        this.result=false;
        final String[] message = new String[1];
        // This code sends volley request and listens to response
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, succesListener, errorListener);

        ComplaintSystem.getInstance().addToRequestQueue(jsObjRequest);
        Log.d("responseLogin","Yolo"+ this.result);
    }

    public void getmycomplaints(Context c,Response.Listener succesListener,Response.ErrorListener errorListener,String username){
        // Instantiate the RequestQueue.
        //RequestQueue queue=Volley.newRequestQueue(c);

        String url ="http://"+this.IP_Addr+":"+this.port+"/users/"+username+"/complaints/";
        User user;
        // resp.setResponse_class();
        Log.d("responseURL",url);
        this.result=false;
        final String[] message = new String[1];
        // This code sends volley request and listens to response
        JsonArrayRequest jsArrRequest = new JsonArrayRequest(Request.Method.GET, url, null, succesListener, errorListener);

        ComplaintSystem.getInstance().addToRequestQueue(jsArrRequest);
        Log.d("responsemycomplaint","Yolo"+ this.result);
    }

    public void populateComplaints(Context c,Response.Listener succesListener,Response.ErrorListener errorListener){
        // Instantiate the RequestQueue.
        //RequestQueue queue=Volley.newRequestQueue(c);

        String url ="http://"+this.IP_Addr+":"+this.port+"/complaints/";
        User user;
        // resp.setResponse_class();
        Log.d("responseURL",url);
        this.result=false;
        final String[] message = new String[1];
        // This code sends volley request and listens to response
        JsonArrayRequest jsArrRequest = new JsonArrayRequest(Request.Method.GET, url, null, succesListener, errorListener);

        ComplaintSystem.getInstance().addToRequestQueue(jsArrRequest);
        Log.d("responsecomplaint","Yolo"+ this.result);
    }
    public void getNotifications(Context c,Response.Listener succesListener,Response.ErrorListener errorListener){
        // Instantiate the RequestQueue.
        //RequestQueue queue=Volley.newRequestQueue(c);

        String url ="http://"+this.IP_Addr+":"+this.port+"/default/notifications.json";

        // resp.setResponse_class();
        Log.d("responseURL",url);
        this.result=false;
        final String[] message = new String[1];
        // This code sends volley request and listens to response
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, succesListener, errorListener);

        ComplaintSystem.getInstance().addToRequestQueue(jsObjRequest);
        Log.d("responseLogin","Yolo"+ this.result);
    }

    public void getAllGrades(Context c,Response.Listener succesListener,Response.ErrorListener errorListener){
        // Instantiate the RequestQueue.
        //RequestQueue queue=Volley.newRequestQueue(c);

        String url ="http://"+this.IP_Addr+":"+this.port+"/default/grades.json";

        // resp.setResponse_class();
        Log.d("responseURL",url);
        this.result=false;
        final String[] message = new String[1];
        // This code sends volley request and listens to response
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, succesListener, errorListener);

        ComplaintSystem.getInstance().addToRequestQueue(jsObjRequest);
        Log.d("responseLogin","Yolo"+ this.result);
    }


    public JsonObjectRequest populateCourseList(){



        String url ="http://"+this.IP_Addr+":"+this.port+"/courses/list.json";

        Log.d("responseURL",url);
        final int[] success = {0};
        final String[] message = new String[1];
        // This code sends volley request and listens to response

        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                        String responseSuccess;

                        String responseMessage = "";
                        try {

//                            responseSuccess = response.getString("success");
//                            message[0] =responseSuccess;
//                            Log.d("response", (response).toString());
//
//                            if (responseSuccess.equals("true")) {
//                                success[0] =1;
//                                Log.d("result", "success");
//                            } else {
//                                Log.d("result", "failure");
//                            }
                            Log.d("responseCourses", response.toString());

                        } catch (Throwable t) {

                        }
                        //behave according to the recieved response code
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("responseError", "C" + error.toString());

                    }
                });

//        jsObjRequest.setRetryPolicy(new RetryPolicy() {
//            @Override
//            public int getCurrentTimeout() {
//                return 50000;
//            }
//
//            @Override
//            public int getCurrentRetryCount() {
//                return 50000;
//            }
//
//            @Override
//            public void retry(VolleyError error) throws VolleyError {
//
//            }
//        });


        //  Log.d("responseSuccess", String.valueOf(success[0]) + "    " +message[0]);
        ComplaintSystem.getInstance().addToRequestQueue(jsObjRequest);//queue.add(jsObjRequest);
        return jsObjRequest;


    }

    public void logoutresponse(Context c,Response.Listener succesListener,Response.ErrorListener errorListener){
        // Instantiate the RequestQueue.
        //RequestQueue queue=Volley.newRequestQueue(c);

        String url ="http://"+this.IP_Addr+":"+this.port+"/default/logout.json";

        // resp.setResponse_class();
        Log.d("responseURL",url);
        this.result=false;
        final String[] message = new String[1];
        // This code sends volley request and listens to response
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, succesListener, errorListener);

        ComplaintSystem.getInstance().addToRequestQueue(jsObjRequest);
        Log.d("responseLogin","Yolo"+ this.result);
    }

    public void getAssignment(String courseCode,Context c,Response.Listener succesListener,Response.ErrorListener errorListener){
        // Instantiate the RequestQueue.
        //RequestQueue queue=Volley.newRequestQueue(c);

        String url ="http://"+this.IP_Addr+":"+this.port+"/courses/course.json/"+courseCode+"/assignments";

        // resp.setResponse_class();
        Log.d("responseURL",url);
        this.result=false;
        final String[] message = new String[1];
        // This code sends volley request and listens to response
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, succesListener, errorListener);

        ComplaintSystem.getInstance().addToRequestQueue(jsObjRequest);
        Log.d("responseLogin","Yolo"+ this.result);
    }

    public void getGrade(String courseCode,Context c,Response.Listener succesListener,Response.ErrorListener errorListener){
        // Instantiate the RequestQueue.
        //RequestQueue queue=Volley.newRequestQueue(c);

        String url ="http://"+this.IP_Addr+":"+this.port+"/courses/course.json/"+courseCode+"/grades";

        // resp.setResponse_class();
        Log.d("responseURL",url);
        this.result=false;
        final String[] message = new String[1];
        // This code sends volley request and listens to response
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, succesListener, errorListener);

        ComplaintSystem.getInstance().addToRequestQueue(jsObjRequest);
        Log.d("responseLogin","Yolo"+ this.result);
    }

    public void getThreads(String courseCode,Context c,Response.Listener succesListener,Response.ErrorListener errorListener){
        // Instantiate the RequestQueue.
        //RequestQueue queue=Volley.newRequestQueue(c);

        String url ="http://"+this.IP_Addr+":"+this.port+"/courses/course.json/"+courseCode+"/threads";

        // resp.setResponse_class();
        Log.d("responseURL",url);
        this.result=false;
        final String[] message = new String[1];
        // This code sends volley request and listens to response
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, succesListener, errorListener);

        ComplaintSystem.getInstance().addToRequestQueue(jsObjRequest);
        Log.d("responseLogin","Yolo"+ this.result);
    }

    public void postThreads(final String title, final String description, final String courseCode,Context c,Response.Listener succesListener,Response.ErrorListener errorListener){
        // Instantiate the RequestQueue.
        //RequestQueue queue=Volley.newRequestQueue(c);

        String url ="http://"+this.IP_Addr+":"+this.port+"/threads/new.json";

        // resp.setResponse_class();
        Log.d("responseURL",url);
        this.result=false;
        final String[] message = new String[1];
        // This code sends volley request and listens to response
        StringRequest jsObjRequest = new StringRequest
                (Request.Method.POST, url, succesListener, errorListener){
            @Override
            protected Map<String, String> getParams(){
                final HashMap<String, String> params = new HashMap<String, String>();
                params.put("title",title);
                params.put("description",description);
                params.put("course_code", courseCode);
                Log.d("params", params.toString());
                return params;
            }
        };

        ComplaintSystem.getInstance().addToRequestQueue(jsObjRequest);
        Log.d("responseLogin","Yolo"+ this.result);
    }

    public void getThread(int threadid,Context c,Response.Listener succesListener,Response.ErrorListener errorListener){
        // Instantiate the RequestQueue.
        //RequestQueue queue=Volley.newRequestQueue(c);

        String url ="http://"+this.IP_Addr+":"+this.port+"/threads/thread.json/"+threadid;

        // resp.setResponse_class();
        Log.d("responseURL",url);
        this.result=false;
        final String[] message = new String[1];
        // This code sends volley request and listens to response
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, succesListener, errorListener);

        ComplaintSystem.getInstance().addToRequestQueue(jsObjRequest);
        Log.d("responseLogin","Yolo"+ this.result);
    }


    public void postComments(final int threadID, final String description,Context c,Response.Listener succesListener,Response.ErrorListener errorListener){
        // Instantiate the RequestQueue.
        //RequestQueue queue=Volley.newRequestQueue(c);

//        String url ="http://"+this.IP_Addr+":"+this.port+"/threads/post_comment.json";
//
//        // resp.setResponse_class();
//        Log.d("responseURL",url);
//        this.result=false;
//        final String[] message = new String[1];
//        // This code sends volley request and listens to response
//        StringRequest jsObjRequest = new StringRequest
//                (Request.Method.POST, url, succesListener, errorListener){
//            @Override
//            protected Map<String, String> getParams(){
//                final HashMap<String, String> params = new HashMap<String, String>();
//                params.put("thread_id",String.valueOf(threadID));
//                params.put("description",description);
//                Log.d("params", params.toString());
//                return params;
//            }
//        };
//
//        MoodlePlus.getInstance().addToRequestQueue(jsObjRequest);
//        Log.d("responseLogin","Yolo"+ this.result);
    }




}
