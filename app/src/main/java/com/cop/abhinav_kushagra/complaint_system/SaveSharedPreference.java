package com.cop.abhinav_kushagra.complaint_system;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

public class SaveSharedPreference
{
    static final String PREF_USER_NAME= "";
    static final String PREF_PASSWORD="";
    static final String PREF_UN_PASS="";

    static SharedPreferences getSharedPreferences(Context ctx) {
        return PreferenceManager.getDefaultSharedPreferences(ctx);
        //  return ctx.getSharedPreferences("Credential",Context.MODE_PRIVATE);
    }

    public static void setUserName(Context ctx, String userName)
    {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(PREF_USER_NAME, userName);
        editor.commit();
    }

    public static void setPassword(Context ctx, String password)
    {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(PREF_PASSWORD, password);
        editor.commit();
    }

    public static void setUsernamePassword(Context ctx, String username,String password) {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(PREF_PASSWORD, password).putString(PREF_USER_NAME, username).putString(PREF_UN_PASS,username+","+password).commit();
        Log.d("shared  prefffffffff", getSharedPreferences(ctx).getString(PREF_USER_NAME, "") + getSharedPreferences(ctx).getString(PREF_PASSWORD, ""));
    }



    public static String getUserName(Context ctx)
    {
        return getSharedPreferences(ctx).getString(PREF_USER_NAME, "");
    }
    public static String getPassword(Context ctx)
    {
        return getSharedPreferences(ctx).getString(PREF_PASSWORD, "");
    }
    public  static String getPrefUnPass(Context ctx){
        return  getSharedPreferences(ctx).getString(PREF_UN_PASS,"");
    }




    public static void clear(Context ctx)
    {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.clear(); //clear all stored data
        editor.commit();
    }
}